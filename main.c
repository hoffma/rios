/**************************************************

file: main.c
purpose: simple RIOS example.
source: https://www.cs.ucr.edu/~vahid/rios/

**************************************************/



#include <stdio.h>
#include <stdlib.h>

#include "timer.h"


typedef struct task {
	unsigned long period;
	unsigned long elapsedTime;
	void (*TickFct)(void);
} task;

task tasks[3];
const unsigned char tasksNum = 3;
const unsigned long tasksPeriodGCD = 200;
const unsigned long periodToggle   = 1000;
const unsigned long periodSequence = 200;

int B0, B2, B3, B4;

void TickFct_Toggle(void);
void TickFct_Sequence(void);
void TickFct_PrintOutputs(void);

unsigned char TimerFlag = 0;
void TimerISR(void) {
	if(TimerFlag) {
		printf("Timer ticked before task processing done.\n");
	} else {
		TimerFlag = 1;
	}
	
	return;
}

int main(void) {
	unsigned char i = 0;
	tasks[i].period 		= periodSequence;
	tasks[i].elapsedTime 	= tasks[i].period;
	tasks[i].TickFct		= &TickFct_Sequence;
	++i;
	tasks[i].period 		= periodSequence;
	tasks[i].elapsedTime 	= tasks[i].period;
	tasks[i].TickFct		= &TickFct_Toggle;
	++i;
	tasks[i].period			= periodSequence;
	tasks[i].elapsedTime	= tasks[i].period;
	tasks[i].TickFct		= &TickFct_PrintOutputs;
	
	if(start_timer(tasksPeriodGCD, &TimerISR))
	{
		printf("\n timer error\n");
		return(1);
	}
	
	printf("\npress ctl-c to quit.\n");
	
	while(1) {
		for(i = 0; i < tasksNum; ++i) {
			if(tasks[i].elapsedTime >= tasks[i].period) {
				tasks[i].TickFct();
				tasks[i].elapsedTime = 0;
			}
			tasks[i].elapsedTime += tasksPeriodGCD;
		}
		TimerFlag = 0;
		while(!TimerFlag) {
			usleep(50000);
		}
	}
}

void TickFct_PrintOutputs(void) {
	printf("B0: %01d, [B2, B3, B4]: [%01d, %01d, %01d]\n", B0, B2, B3, B4);
	
}

void TickFct_Toggle(void) {
	static unsigned char init = 1;
	if(init) {
		B0 = 0;
		init = 0;
	} else {
		B0 = !B0;
	}
}

void TickFct_Sequence(void) {
	static unsigned char init = 1;
	unsigned char tmp = 0;
	if (init) { // Initialization behavior
		init = 0;
		B2   = 1;
		B3   = 0;
		B4   = 0;
	}
	else { // Normal behavior
		tmp = B4;
		B4  = B3;
		B3  = B2;
		B2  = tmp;
}
}



