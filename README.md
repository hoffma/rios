# RIOS
 
Recreated RIOS from: https://www.cs.ucr.edu/~vahid/rios/ since the header file is missing in the example. Added a simple timer which works for Windows and Linux.

    RIOS is a task scheduler written entirely in C that:

    Is simple and understandable for the beginning embedded programmer
    Can provide basic non-preemptive or preemptive multitasking capabilities for cooperative tasks
    Requires only a few dozen lines of C code. Reduces need for RTOS (real-time operating system).
