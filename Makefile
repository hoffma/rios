
TARGET=rios

CC=gcc

CC_FLAGS=
EFFORT=-O3
WARNINGS =
# WARNINGS = -Wshadow -Wundef -Wpointer-arith -Wcast-qual -Wcast-align -Wwrite-strings -Wredundant-decls

SRC=$(wildcard *.c)
OBJ=$(SRC:.c=.o)

all: $(OBJ)
	$(CC) -o $(TARGET) $?

%.o: %.c
	# $(CC) -c $(CC_FLAGS) $(EFFORT) -std=c11 $(WARNINGS) -o $@ $<
	$(CC) -c $(CC_FLAGS) $(EFFORT) $(WARNINGS) -o $@ $<

clean:
	rm -f $(TARGET)
	rm -f *.o
